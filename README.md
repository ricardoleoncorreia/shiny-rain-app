# Shiny Rain App

Hyper-parameters optimization is an important step to build Machine Learning solutions. Random Forest Grid Search simulator helps the user to understand how the results of a classification model are being affected after changing the algorithm hyper-parameters.

## Shiny app backend characteristics:

* **Machine Learning Model:** Random Forest.
* **Dataset:** Subset of 20.000 random observations from Australia Rain dataset.
* **Train/test split:** 80% training - 20% test.
* **Chosen predictors:** MaxTemp and Humidity3pm.
* **Hyper-parameters to tune up:** number of trees and maximum tree depth.

The report is part of the projects of the [Data Science Specialization](https://www.coursera.org/specializations/jhu-data-science)
courses at Johns Hopkins University.
